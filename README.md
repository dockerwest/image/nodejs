NodeJS image
============

NodeJS images with ssh, git, npm and yarn installed where its possible

Versions
--------

The following versions are available:
- 4 (ssh, git, npm)
- 6 (ssh, git, npm, yarn)
- 8 (ssh, git, npm, yarn)
- 10 (ssh, git, npm, yarn)
- 12 (ssh, git, npm, yarn)
- 14 (ssh, git, npm, yarn)

License
-------

MIT License (MIT). See [License File](LICENSE.md) for more information.
