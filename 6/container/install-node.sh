#!/bin/sh

set -e

apk --update --no-cache upgrade
apk --update --no-cache add g++ gcc libgcc linux-headers make python
apk --update --no-cache add nodejs nodejs-npm su-exec openssh git yarn

mkdir -p /nodeapp
