#!/bin/sh

set -e

apk --update --no-cache upgrade
apk --update --no-cache add g++ gcc libgcc linux-headers make python3
apk --update --no-cache add nodejs npm su-exec openssh git yarn

mkdir -p /nodeapp
