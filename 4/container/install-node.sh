#!/bin/sh

set -e

apk --update --no-cache upgrade
apk --update --no-cache add g++ gcc libgcc linux-headers make python
apk --update --no-cache add nodejs-lts su-exec openssh git

mkdir -p /nodeapp
